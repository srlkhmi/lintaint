LinTaint is a collection of Pintools to identify environment detection techniques used in Linux malware. The three Pintools are timebomb, instruction based and files based.


1. Timebomb

Sometimes malware uses the time to evade the analysis. It waits for some time to start execution or start execution on specific date or time. To identify similar evasion techniques, LinTaint employs a time bomb detection tool. The discovery of time bomb based on the time-related system calls. The time system call returns the value of time in seconds since the Epoch. The return values are stored in the register EAX. We performed analysis on a toy-program and were able to identify a ’user-selectable threshold’ that could act as a threshold. Working with this, to detect the time bomb, the cmp instructions instrumented. The tool calculates the total number of instructions and the number of instructions after the last tainted cmp instruction. This value is compared against the user-selectable threshold that we have identified and if Value is less than user-selectable threshold, Time bomb is detected. 

2. Instructions

The instruction based Pin tool mainly focuses on instructions cpuid, RDTSC, IN, SDIT, SGDT and SLDT. The trick used to detect the anti-VM techniques is to check whether the malware exits soon just after the collection of the specific information. To detect whether the malware exits early, two counters are used. One counts the total number of instructions and the second counter starts the counting after the last tainted cmp instruction. Calculates the percentage and this Perc compared against the user-selectable threshold that we have identified, and if Value is greater than user-selectable threshold, anti-VM detection technique is detected.

3. Files

Linux operating system uses files to store data and malware can easily read these files to understand the environment information. In this Pin tool, the open system call is hooked to get the file descriptors of the interesting files. The values that are read from these interesting files are tainted and tracks the tag propagation. If the tainted information is compared, that information is printed in an output file to understand technique used in environment detection. After these comparisons, if the malware exits early then this information is used as a VM detection method.
