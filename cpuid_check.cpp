#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h> 
#include <errno.h>
#include <inttypes.h>
#include <iostream>
#include <fstream>
#include <map>
#include <iomanip>
#include <algorithm>
#include <vector>
#include <set>
#include <sstream>
#include "pin.H"
#include "libdft_api.h"
#include "libdft_core.h"
#include "syscall_desc.h"
#include "tagmap_custom.h"
#include "tagmap.h"
#include "tag_traits.h"
#include "branch_pred.h"
#include "filenames.h"

#define DLIB_SUFF	".so"
#define DLIB_SUFF_ALT	".so."
#define WORD_LEN	4

extern syscall_desc_t syscall_desc[SYSCALL_MAX];
/* ins descriptors */
extern ins_desc_t ins_desc[XED_ICLASS_LAST];
//Descriptors
static set<int> fdset;

UINT32 	n;

string s[4];
int i;
int j=0;
static int flag=0;
int flag_rdtsc=0;

static UINT64 ins_count = 0;
static UINT64 ins_count_exit = 0;
static UINT64 ins_count_vendorflag = 0;
static UINT64 ins_count_rdtsc_ins = 0;

int count_rdtsc=0;
int count_cpuid=0;


int vendor_flag=0;


std::ofstream TraceFile;

std::map <ADDRINT, std::string> instructions;

// Required for getting the context.
extern REG thread_ctx_ptr;

// Shorthand for accessing register taint. thread_ctx is expected
// to be defined in the context where the shorthand is used.
#define RTAG thread_ctx->vcpu.gpr

KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool",
    "o", "cpuid_check_output.txt", "specify output file name");
    

/*-------------------------------------------------Instruction count----------------*/
VOID ins_cout_exit() 
{ 
	ins_count_exit++; 
}

VOID ins_count_tainted_cmp() 
{ 
	ins_count++; 
}

VOID ins_count_vendor_flag() 
{ 
	ins_count_vendorflag++; 
}


VOID ins_count_rdtsc() 
{ 
	ins_count_rdtsc_ins++; 
}


VOID Instruction(INS ins, VOID *v)
{
    // Insert a call to docount before every instruction, no arguments are passed
    
    INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)ins_cout_exit, IARG_END);
    
    if(flag==1)
    {
    INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)ins_count_tainted_cmp, IARG_END);
    }
    
    if(vendor_flag==1)
    {
       INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)ins_count_vendor_flag, IARG_END);
    }
    
      
    if(flag_rdtsc==1)
    {
       INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)ins_count_rdtsc, IARG_END);
    }
}


/*-------------------------------------------------Sets the flag----------------*/

void set_flag(thread_ctx_t *thread_ctx,  uint32_t dst)
{
j=0;

for (i=0;i<4;i++)
{
	s[i]=tag_sprint(RTAG[dst][i]);

	if((s[i]=="{777}")||(s[i]=="{111}")||(s[i]=="{888}")||(s[i]=="{666}")||(s[i]=="{555}")||(s[i]=="{444}"))
		{
			 j++;	
		}	
}

if(flag==0&&j==4)
{
  flag=4;
}
}

void set_flag_w(thread_ctx_t *thread_ctx,  uint32_t dst)
{
j=0;

for (i=0;i<2;i++)
{
	s[i]=tag_sprint(RTAG[dst][i]);

	if((s[i]=="{777}")||(s[i]=="{111}")||(s[i]=="{888}")||(s[i]=="{666}")||(s[i]=="{555}")||(s[i]=="{444}"))
		{
			 j++;	
		}	
}

if(flag==0&&j==2)
{
  flag=2;
}
}

void set_flag_b(thread_ctx_t *thread_ctx,  uint32_t dst)
{
j=0;

for (i=0;i<1;i++)
{
	s[i]=tag_sprint(RTAG[dst][i]);

	if((s[i]=="{777}")||(s[i]=="{111}")||(s[i]=="{888}")||(s[i]=="{666}")||(s[i]=="{555}")||(s[i]=="{444}"))
		{
			 j++;	
		}	
}

if(flag==0&&j==1)
{
  flag=1;
}


}

/*-------------------------------------------------Tainting cpuid and rdtsc----------------*/
static void PIN_FAST_ANALYSIS_CALL
rdtsc_print_reg(ADDRINT ins_address,const CONTEXT *ctx)
{

thread_ctx_t *thread_ctx = (thread_ctx_t *)PIN_GetContextReg((CONTEXT *)ctx, thread_ctx_ptr);
 		
 	RTAG[GPR_EAX][0] = {888};
	RTAG[GPR_EAX][1] = {888};
	RTAG[GPR_EAX][2] = {888};
	RTAG[GPR_EAX][3] = {888};
		
	RTAG[GPR_EDX][0] = {111};
	RTAG[GPR_EDX][1] = {111};
	RTAG[GPR_EDX][2] = {111};
	RTAG[GPR_EDX][3] = {111};
	     
}



static void PIN_FAST_ANALYSIS_CALL
cpuid_print_reg(ADDRINT ins_address,const CONTEXT *ctx)
{
thread_ctx_t *thread_ctx = (thread_ctx_t *)PIN_GetContextReg((CONTEXT *)ctx, thread_ctx_ptr);
		
 	RTAG[GPR_EAX][0] = {777};
	RTAG[GPR_EAX][1] = {777};
	RTAG[GPR_EAX][2] = {777};
	RTAG[GPR_EAX][3] = {777};
		
	RTAG[GPR_EBX][0] = {666};
	RTAG[GPR_EBX][1] = {666};
	RTAG[GPR_EBX][2] = {666};
	RTAG[GPR_EBX][3] = {666};
	
	RTAG[GPR_ECX][0] = {555};
	RTAG[GPR_ECX][1] = {555};
	RTAG[GPR_ECX][2] = {555};
	RTAG[GPR_ECX][3] = {555};
	
	RTAG[GPR_EDX][0] = {444};
	RTAG[GPR_EDX][1] = {444};
	RTAG[GPR_EDX][2] = {444};
	RTAG[GPR_EDX][3] = {444};
	     
}

/*-------------------------------------------------Prints the values of foperands in cmp----------------*/
static void PIN_FAST_ANALYSIS_CALL
r2r_print_tag(ADDRINT ins_address, thread_ctx_t *thread_ctx, uint32_t dst, uint32_t dst_val, uint32_t src, uint32_t src_val)
{

set_flag(thread_ctx,dst);
set_flag(thread_ctx,src);
}

static void PIN_FAST_ANALYSIS_CALL
r2m_print_tag(ADDRINT ins_address, thread_ctx_t *thread_ctx, uint32_t dst, uint32_t dst_val, ADDRINT src)
{
set_flag(thread_ctx,dst);
}

static void PIN_FAST_ANALYSIS_CALL
r2i_print_tag(ADDRINT ins_address, thread_ctx_t *thread_ctx,  uint32_t dst, uint32_t dst_val, ADDRINT imm_val)
{
set_flag(thread_ctx,dst);

if(imm_val==0x756e6547|| imm_val==0x6c65746e ||imm_val== 0x49656e69) //Get vendor check
{
  vendor_flag=1;
}
		
}

static void PIN_FAST_ANALYSIS_CALL
r2r_print_tag_w(ADDRINT ins_address, thread_ctx_t *thread_ctx, uint32_t dst, uint16_t dst_val, uint32_t src, uint16_t src_val)
{
set_flag_w(thread_ctx,dst);

}


static void PIN_FAST_ANALYSIS_CALL
r2i_print_tag_w(ADDRINT ins_address, thread_ctx_t *thread_ctx,  uint32_t dst, uint16_t dst_val, ADDRINT imm_val)
{
set_flag_w(thread_ctx,dst);


if(imm_val==0x756e6547|| imm_val==0x6c65746e ||imm_val== 0x49656e69) //Get vendor check
{
  vendor_flag=1;
}
      
}

static void PIN_FAST_ANALYSIS_CALL
r2m_print_tag_w(ADDRINT ins_address, thread_ctx_t *thread_ctx, uint32_t dst, uint16_t dst_val, ADDRINT src)
{
set_flag_w(thread_ctx,dst);

}


static void PIN_FAST_ANALYSIS_CALL
r2r_print_tag_opb_l(ADDRINT ins_address, thread_ctx_t *thread_ctx, uint32_t dst, uint8_t dst_val, uint32_t src, uint8_t src_val)
{
set_flag_b(thread_ctx,dst);

}

static void PIN_FAST_ANALYSIS_CALL
r2i_print_tag_8_bl(ADDRINT ins_address, thread_ctx_t *thread_ctx,  uint32_t dst, uint8_t dst_val, ADDRINT imm_val)
{
set_flag_b(thread_ctx,dst);
if(imm_val==0x756e6547|| imm_val==0x6c65746e || imm_val==0x49656e69) //Get vendor check
{
  vendor_flag=1;
}

}

static void PIN_FAST_ANALYSIS_CALL
r2m_print_tag_8_bl(ADDRINT ins_address, thread_ctx_t *thread_ctx, uint32_t dst, uint8_t dst_val, ADDRINT src)
{
set_flag_b(thread_ctx,dst);
}



static void PIN_FAST_ANALYSIS_CALL
m2i_print_tag_l(ADDRINT ins_address, ADDRINT src, ADDRINT imm_val)
{

}

static void PIN_FAST_ANALYSIS_CALL
m2i_print_tag_w(ADDRINT ins_address, ADDRINT src, ADDRINT imm_val)
{
}
static void PIN_FAST_ANALYSIS_CALL
m2i_print_tag_b(ADDRINT ins_address, ADDRINT src, ADDRINT imm_val)
{

}

/*--------------- Instrument instruction cmp,rdtsc,cpuid and In--------------------------*/

void dta_instrument_in(INS ins)
{
 cout<<"Instruction IN detected"<<endl;
}

void dta_instrument_sidt(INS ins)
{
 cout<<"Instruction SIDT detected"<<endl;
}

void dta_instrument_sgdt(INS ins)
{
 cout<<"Instruction SGDT detected"<<endl;
}

void dta_instrument_sldt(INS ins)
{
 cout<<"Instruction SLDT detected"<<endl;
}


void dta_instrument_rdtsc(INS ins)
{
	
 	INS_InsertCall(ins,
                   IPOINT_BEFORE,
                   (AFUNPTR)rdtsc_print_reg,
                   IARG_FAST_ANALYSIS_CALL,
				   IARG_INST_PTR,
                   IARG_CONST_CONTEXT,
                   IARG_END);
 
	count_rdtsc++;
	ins_count_rdtsc_ins=0;
	flag_rdtsc=1;
 
}

void dta_instrument_cpuid(INS ins)
{
	
	INS_InsertCall(ins,
                   IPOINT_BEFORE,
                   (AFUNPTR)cpuid_print_reg,
                   IARG_FAST_ANALYSIS_CALL,
	               IARG_INST_PTR,
                   IARG_CONST_CONTEXT,
                   IARG_END);
                   
	count_cpuid++;	
}


void dta_instrument_cmp(INS ins)
{
	REG reg_dst, reg_src;
	flag=0;
	if (INS_OperandIsReg(ins, OP_0))
		{
			reg_dst = INS_OperandReg(ins, OP_0);
			if (REG_is_gr32(reg_dst)) 
				{
					  /* second operand is register */
					  if (INS_OperandIsReg(ins, OP_1))
						{
						    /* case: cmp reg reg */ 
							reg_src = INS_OperandReg(ins, OP_1);
				            INS_InsertCall(ins,
                                                IPOINT_BEFORE,
                                                (AFUNPTR)r2r_print_tag,
                                                IARG_FAST_ANALYSIS_CALL,
												IARG_INST_PTR,
                                                IARG_REG_VALUE, thread_ctx_ptr,
                                                IARG_UINT32, REG32_INDX(reg_dst),
                                                IARG_REG_VALUE, reg_dst,
                                                IARG_UINT32, REG32_INDX(reg_src),
                                                IARG_REG_VALUE, reg_src,
                                                IARG_END);
						}
						/* 	case: cmp reg,imm */
				 	 else if (INS_OperandIsImmediate(ins, OP_1))
				  		{
							INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)r2i_print_tag,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_REG_VALUE, thread_ctx_ptr,
								IARG_UINT32, REG32_INDX(reg_dst),
								IARG_REG_VALUE, reg_dst,
								IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
								IARG_END);

				  		}
						/* case: cmp reg,mem  */
				 	else if(INS_OperandIsMemory(ins, OP_1))
				   		{
							INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)r2m_print_tag,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_REG_VALUE, thread_ctx_ptr,
								IARG_UINT32, REG32_INDX(reg_dst),
								IARG_REG_VALUE, reg_dst,
								IARG_MEMORYREAD_EA,
								IARG_END);
				  	   }
				}
			else if (REG_is_gr16(reg_dst)) 
				{
					  if (INS_OperandIsReg(ins, OP_1))
					  	{
							reg_src = INS_OperandReg(ins, OP_1);
							/* cmp reg reg case */
			         	    INS_InsertCall(ins,
                                                IPOINT_BEFORE,
                                                (AFUNPTR)r2r_print_tag_w,
                                                IARG_FAST_ANALYSIS_CALL,
												IARG_INST_PTR,
                                                IARG_REG_VALUE, thread_ctx_ptr,
                                                IARG_UINT32, REG16_INDX(reg_dst),
                                                IARG_REG_VALUE, reg_dst,
                                                IARG_UINT32, REG16_INDX(reg_src),
                                                IARG_REG_VALUE, reg_src,
                                                IARG_END);
				   		}
				  		/* cmp reg imm case */
				  	else if (INS_OperandIsImmediate(ins, OP_1))
				  		{
						INS_InsertCall(ins,
							IPOINT_BEFORE,
							(AFUNPTR)r2i_print_tag_w,
							IARG_FAST_ANALYSIS_CALL,
							IARG_INST_PTR,
							IARG_REG_VALUE, thread_ctx_ptr,
							IARG_UINT32, REG16_INDX(reg_dst),
							IARG_REG_VALUE, reg_dst,
							IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
							IARG_END);
				  		}
				  		/* cmp reg mem case */
				    else if(INS_OperandIsMemory(ins, OP_1))
				    	{
							INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)r2m_print_tag_w,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_REG_VALUE, thread_ctx_ptr,
								IARG_UINT32, REG16_INDX(reg_dst),
								IARG_REG_VALUE, reg_dst,
								IARG_MEMORYREAD_EA,
								IARG_END);
				  		}
				}
			else if (REG_is_gr8(reg_dst)) 
				{
					  if (INS_OperandIsReg(ins, OP_1))
						{
							reg_src = INS_OperandReg(ins, OP_1);
							/* cmp reg reg case */

							if (REG_is_Lower8(reg_dst) &&
								REG_is_Lower8(reg_src))
								{	
									/* lower 8-bit registers */
									INS_InsertCall(ins,
										IPOINT_BEFORE,
										(AFUNPTR)r2r_print_tag_opb_l,
										IARG_FAST_ANALYSIS_CALL,
										IARG_INST_PTR,
										IARG_REG_VALUE, thread_ctx_ptr,
										IARG_UINT32, REG8_INDX(reg_dst),
										IARG_REG_VALUE, reg_dst,
										IARG_UINT32, REG8_INDX(reg_src),
										IARG_REG_VALUE, reg_src,
										IARG_END);
								}
							else if(REG_is_Upper8(reg_dst) &&
							REG_is_Upper8(reg_src))
								{
									/* upper 8-bit registers */
									INS_InsertCall(ins,
										IPOINT_BEFORE,
										(AFUNPTR)r2r_print_tag_opb_l,
										IARG_FAST_ANALYSIS_CALL,
										IARG_INST_PTR,
										IARG_REG_VALUE, thread_ctx_ptr,
										IARG_UINT32, REG8_INDX(reg_dst),
										IARG_REG_VALUE, reg_dst,
										IARG_UINT32, REG8_INDX(reg_src),
										IARG_REG_VALUE, reg_src,
										IARG_END);
								}
							else if (REG_is_Lower8(reg_dst))
								{
									/* 
						 			* destination register is a
						 			* lower 8-bit register and
						 			* source register is an upper
									* 8-bit register
						 			*/
									INS_InsertCall(ins,
										IPOINT_BEFORE,
										(AFUNPTR)r2r_print_tag_opb_l,
										IARG_FAST_ANALYSIS_CALL,
										IARG_INST_PTR,
										IARG_REG_VALUE, thread_ctx_ptr,
										IARG_UINT32, REG8_INDX(reg_dst),
										IARG_REG_VALUE, reg_dst,
										IARG_UINT32, REG8_INDX(reg_src),
										IARG_REG_VALUE, reg_src,
										IARG_END);
								}
							else
									/* 
						 			* destination register is an
						 			* upper 8-bit register and
						 			* source register is a lower
						 			* 8-bit register
						 			*/
						
						 		{
									INS_InsertCall(ins,
										IPOINT_BEFORE,
										(AFUNPTR)r2r_print_tag_opb_l,
										IARG_FAST_ANALYSIS_CALL,
										IARG_INST_PTR,
										IARG_REG_VALUE, thread_ctx_ptr,
										IARG_UINT32, REG8_INDX(reg_dst),
										IARG_REG_VALUE, reg_dst,
										IARG_UINT32, REG8_INDX(reg_src),
										IARG_REG_VALUE, reg_src,
										IARG_END);
								}
				  }
				  /*
					cmp reg imm case
				 */
				  else if (INS_OperandIsImmediate(ins, OP_1))
						{
							if (REG_is_Lower8(reg_dst))
								{
					  				INS_InsertCall(ins,
									IPOINT_BEFORE,
									(AFUNPTR)r2i_print_tag_8_bl,
									IARG_FAST_ANALYSIS_CALL,
									IARG_INST_PTR,
									IARG_REG_VALUE, thread_ctx_ptr,
									IARG_UINT32, REG8_INDX(reg_dst),
									IARG_REG_VALUE, reg_dst,
									IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
									IARG_END);
				     			}
							else if (REG_is_Upper8(reg_dst))
								{
					  				INS_InsertCall(ins,
									IPOINT_BEFORE,
									(AFUNPTR)r2i_print_tag_8_bl,
									IARG_FAST_ANALYSIS_CALL,
									IARG_INST_PTR,
									IARG_REG_VALUE, thread_ctx_ptr,
									IARG_UINT32, REG8_INDX(reg_dst),
									IARG_REG_VALUE, reg_dst,
									IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
									IARG_END);
				     			}
				  		}
				  /*
					cmp reg mem case
				  */
				  	else if(INS_OperandIsMemory(ins, OP_1))
						{
							if (REG_is_Lower8(reg_dst))
								{
								  INS_InsertCall(ins,
									IPOINT_BEFORE,
									(AFUNPTR)r2m_print_tag_8_bl,
									IARG_FAST_ANALYSIS_CALL,
									IARG_INST_PTR,
									IARG_REG_VALUE, thread_ctx_ptr,
									IARG_UINT32, REG8_INDX(reg_dst),
									IARG_REG_VALUE, reg_dst,
									IARG_MEMORYREAD_EA,
									IARG_END);
					 			}
							if (REG_is_Upper8(reg_dst))
								{
					  				INS_InsertCall(ins,
									IPOINT_BEFORE,
									(AFUNPTR)r2m_print_tag_8_bl,
									IARG_FAST_ANALYSIS_CALL,
									IARG_INST_PTR,
									IARG_REG_VALUE, thread_ctx_ptr,
									IARG_UINT32, REG8_INDX(reg_dst),
									IARG_REG_VALUE, reg_dst,
									IARG_MEMORYREAD_EA,
									IARG_END);
					 			}

				  		}
				}
		}
		else if (INS_OperandIsMemory(ins, OP_0)) {
				//cmp mem reg
				  if (INS_OperandIsReg(ins, OP_1))
				  	{
						reg_src = INS_OperandReg(ins, OP_1);
						if (REG_is_gr32(reg_src))
							 {
								INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)r2m_print_tag,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_REG_VALUE, thread_ctx_ptr,
								IARG_UINT32, REG32_INDX(reg_src),
								IARG_REG_VALUE, reg_src,
								IARG_MEMORYREAD_EA,
								IARG_END);
							}
						else if (REG_is_gr16(reg_src))
							 {
								INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)r2m_print_tag_w,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_REG_VALUE, thread_ctx_ptr,
								IARG_UINT32, REG16_INDX(reg_src),
								IARG_REG_VALUE, reg_src,
								IARG_MEMORYREAD_EA,
								IARG_END);
							}
						else if (REG_is_gr8(reg_src)) 
							{
								if (REG_is_Lower8(reg_src))
									{
						  			 INS_InsertCall(ins,
										IPOINT_BEFORE,
										(AFUNPTR)r2m_print_tag_8_bl,
										IARG_FAST_ANALYSIS_CALL,
										IARG_INST_PTR,
										IARG_REG_VALUE, thread_ctx_ptr,
										IARG_UINT32, REG8_INDX(reg_src),
										IARG_REG_VALUE, reg_src,
										IARG_MEMORYREAD_EA,
										IARG_END);
									}
								else if (REG_is_Upper8(reg_src))
									{
						  
						  			INS_InsertCall(ins,
										IPOINT_BEFORE,
										(AFUNPTR)r2m_print_tag_8_bl,
										IARG_FAST_ANALYSIS_CALL,
										IARG_INST_PTR,
										IARG_REG_VALUE, thread_ctx_ptr,
										IARG_UINT32, REG8_INDX(reg_src),
										IARG_REG_VALUE, reg_src,
										IARG_MEMORYREAD_EA,
										IARG_END);
									}
							}
				  

 				else if (INS_OperandIsImmediate(ins, OP_1))
 					{
					/* 32-bit operand */
						if (INS_OperandWidth(ins, OP_1) == MEM_LONG_LEN)
							{
							
								INS_InsertCall(ins,
									IPOINT_BEFORE,
									(AFUNPTR)m2i_print_tag_l,
									IARG_FAST_ANALYSIS_CALL,
									IARG_INST_PTR,
									IARG_MEMORYREAD_EA,
									IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
									IARG_END);
							}
					/* 16-bit operand */
					else if (INS_OperandWidth(ins, OP_1) == MEM_WORD_LEN)
							{
							INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)m2i_print_tag_w,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_MEMORYREAD_EA,
								IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
								IARG_END);
							}		
					/* 8-bit operand */
					else
						{
							INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)m2i_print_tag_b,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_MEMORYREAD_EA,
								IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
								IARG_END);
						}
				  }
			}
	}			  			

}
/*------------------------------------------------------------------------------------------------------------*/

void Fini(INT32 cde, void *v)
{
    double b=0;
	TraceFile<<"Total number of instruction  =  "<<ins_count_exit<<endl;
	TraceFile<<"Number of rtdc  =  "<<count_rdtsc<<endl;
	TraceFile<<"Instruction count after rdtsc  =  "<<ins_count_rdtsc_ins<<endl;
	TraceFile<<"Number of CPUID =  "<<count_cpuid<<endl;
	TraceFile<<" Vendor flag count = "<<ins_count_vendorflag<<endl;
	if(vendor_flag==1)
		{
		  TraceFile<<" Environment detected using CPUID instruction"<<endl;
			
		}
    		
		b=((double)ins_count_rdtsc_ins / (double)ins_count_exit)*100;
    printf("%lf\n",b);
	
	if(b<2)
	{
		cout<<"Malicious"<<endl;
		TraceFile<<"Malicious"<<endl;
	}
	else
	{
		cout<<"Not Malicious"<<endl;
		TraceFile<<"Not Malicious"<<endl;
	}
	
	TraceFile<< "The percentage is "<<b<<endl;
    
	TraceFile.close();
}

int main(int argc, char * argv[])
{   
    
    if (PIN_Init(argc, argv)) return 1;
    PIN_InitSymbols(); /* initialize symbol processing */ 
	
    
    if (unlikely(libdft_init() != 0))
		return 1;
    
    TraceFile.open(KnobOutputFile.Value().c_str());
    //TraceFile << hex;
    TraceFile.setf(ios::showbase);  
    
    INS_AddInstrumentFunction(Instruction,0); 
         
    (void)ins_set_post(&ins_desc[XED_ICLASS_CMP], dta_instrument_cmp);
    (void)ins_set_post(&ins_desc[XED_ICLASS_TEST], dta_instrument_cmp);
    (void)ins_set_post(&ins_desc[XED_ICLASS_IN], dta_instrument_in);
    (void)ins_set_post(&ins_desc[XED_ICLASS_CPUID], dta_instrument_cpuid);
    (void)ins_set_post(&ins_desc[XED_ICLASS_RDTSC], dta_instrument_rdtsc);
    (void)ins_set_post(&ins_desc[XED_ICLASS_SIDT], dta_instrument_sidt);
    (void)ins_set_post(&ins_desc[XED_ICLASS_SGDT], dta_instrument_sgdt);
    (void)ins_set_post(&ins_desc[XED_ICLASS_SLDT], dta_instrument_sldt);
   
    PIN_AddFiniFunction(Fini,0);
    PIN_StartProgram();
 
    libdft_die(); 
    return 0;
	
}
