#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h> 
#include <errno.h>
#include <inttypes.h>
#include <iostream>
#include <fstream>
#include <map>
#include <iomanip>
#include <algorithm>
#include <vector>
#include <set>
#include "pin.H"
#include "libdft_api.h"
#include "libdft_core.h"
#include "syscall_desc.h"
#include "tagmap_custom.h"
#include "tagmap.h"
#include "tag_traits.h"
#include "branch_pred.h"
#include "filenames.h"

#define DLIB_SUFF	".so"
#define DLIB_SUFF_ALT	".so."
#define WORD_LEN	4

extern syscall_desc_t syscall_desc[SYSCALL_MAX];
/* ins descriptors */
extern ins_desc_t ins_desc[XED_ICLASS_LAST];
UINT32 	n;
//Descriptors
static set<int> fdset;

std::map <ADDRINT, std::string> instructions;

static UINT64 ins_count = 0;
static UINT64 ins_count_exit = 0;
static int flag=0;

// Required for getting the context.
extern REG thread_ctx_ptr;

std::ofstream OutFile;
KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool",
    "o", "read_hook_output.txt", "specify output file name");
           
string s[4];
int i;
int j=0;

// Shorthand for accessing register taint. thread_ctx is expected
// to be defined in the context where the shorthand is used.
#define RTAG thread_ctx->vcpu.gpr

static KNOB<string> TrackStdin(KNOB_MODE_WRITEONCE, "pintool", "stdin",
	"0", "Taint data originating from stdin.");

static void post_open_hook(syscall_ctx_t *ctx)
{
	/* not successful; optimized branch */
	if (unlikely((long)ctx->ret < 0))
		return;

	
    OutFile<<"Open Argument 0 "<<(char *)ctx->arg[SYSCALL_ARG0]<<endl;
    
    int test=0;
    /* Only include the defined files*/
    if ((strstr((char *)ctx->arg[SYSCALL_ARG0],cpuinfo) != NULL)||(strstr((char *)ctx->arg[SYSCALL_ARG0],sys_vendor) != NULL)||(strstr((char *)ctx->arg[SYSCALL_ARG0],board_vendor) != NULL)||(strstr((char *)ctx->arg[SYSCALL_ARG0],bios_vendor) != NULL)||(strstr((char *)ctx->arg[SYSCALL_ARG0],locale_archive) != NULL)||(strstr((char *)ctx->arg[SYSCALL_ARG0],uptime) != NULL)||(strstr((char *)ctx->arg[SYSCALL_ARG0],route) != NULL)||(strstr((char *)ctx->arg[SYSCALL_ARG0],dmseg) != NULL)||(strstr((char *)ctx->arg[SYSCALL_ARG0],ioports) != NULL)||(strstr((char *)ctx->arg[SYSCALL_ARG0],scsi) != NULL)||(strstr((char *)ctx->arg[SYSCALL_ARG0],dev_null) != NULL)||(strstr((char *)ctx->arg[SYSCALL_ARG0],ostype) != NULL)||(strstr((char *)ctx->arg[SYSCALL_ARG0],osrelease) != NULL)||(strstr((char *)ctx->arg[SYSCALL_ARG0],self_test) != NULL)||(strstr((char *)ctx->arg[SYSCALL_ARG0],hosts) != NULL))
       {
       	test=1;
       }
      
    
	/* ignore dynamic shared libraries and insert fd of interested files*/
	if ((strstr((char *)ctx->arg[SYSCALL_ARG0], DLIB_SUFF) == NULL &&
		strstr((char *)ctx->arg[SYSCALL_ARG0], DLIB_SUFF_ALT) == NULL)&&(test==1))
		{
			
			fdset.insert((int)ctx->ret);
		}
		
	
	
}

static void post_close_hook(syscall_ctx_t *ctx)
{
	/* iterator */
	set<int>::iterator it;

	/* not successful; optimized branch */
	if (unlikely((long)ctx->ret < 0))
		return;
	
	/*
	 * if the descriptor (argument) is
	 * interesting, remove it from the
	 * monitored set
	 */
	it = fdset.find((int)ctx->arg[SYSCALL_ARG0]);
	if (likely(it != fdset.end()))
		fdset.erase(it);
}

void post_read_hook(syscall_ctx_t *ctx) 
{
    
    OutFile<<"Read Argument 1 :"<<(char *)ctx->arg[SYSCALL_ARG1]<<endl; 
    if (unlikely((long)ctx->ret <= 0))
                return;
	
	/* taint-source */
	if (fdset.find(ctx->arg[SYSCALL_ARG0]) != fdset.end())
		{
        	/* set the tag markings */
				
	        tagmap_setn(ctx->arg[SYSCALL_ARG1], (size_t)ctx->ret);
		}
	else
		{
        	/* clear the tag markings */
	        tagmap_clrn(ctx->arg[SYSCALL_ARG1], (size_t)ctx->ret);
    	}

}





VOID ins_cout_exit() 
{ 
	ins_count_exit++; 
}

VOID ins_count_tainted_cmp() 
{ 
	ins_count++; 
}

VOID Instruction(INS ins, VOID *v)
{
    // Insert a call to ins_count_exit before every instruction, no arguments are passed
    
    INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)ins_cout_exit, IARG_END);
    
    if(flag==1)
    {
    INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)ins_count_tainted_cmp, IARG_END);
    }
}

void set_flag_imm(thread_ctx_t *thread_ctx,  uint32_t dst,ADDRINT imm_val)
{
j=0;
if(imm_val!=4294967295)
{
for (i=0;i<4;i++)
{
	s[i]=tag_sprint(RTAG[dst][i]);

	if(s[i]=="{1}")
		{
			 j++;	
		}	
}
}
if(flag==0&&j==4)
{
  flag=1;
}
}

void set_flag_imm_w(thread_ctx_t *thread_ctx,  uint32_t dst,ADDRINT imm_val)
{
j=0;
if(imm_val!=4294967295)
{
for (i=0;i<2;i++)
{
	s[i]=tag_sprint(RTAG[dst][i]);

	if(s[i]=="{1}")
		{
			 j++;	
		}	
}
}
if(flag==0&&j==2)
{
  flag=1;
}


}

void set_flag_imm_b(thread_ctx_t *thread_ctx,  uint32_t dst,ADDRINT imm_val)
{
j=0;
if(imm_val!=4294967295)
{
for (i=0;i<1;i++)
{
	s[i]=tag_sprint(RTAG[dst][i]);

	if(s[i]=="{1}")
		{
			 j++;	
		}	
}
}
if(flag==0&&j==1)
{
  flag=1;
}
}

void set_flag(thread_ctx_t *thread_ctx,  uint32_t dst)
{
j=0;

for (i=0;i<4;i++)
{
	s[i]=tag_sprint(RTAG[dst][i]);

	if(s[i]=="{1}")
		{
			 j++;	
		}	
}

if(flag==0&&j==4)
{
  flag=1;
}
}

void set_flag_w(thread_ctx_t *thread_ctx,  uint32_t dst)
{
j=0;

for (i=0;i<2;i++)
{
	s[i]=tag_sprint(RTAG[dst][i]);

	if(s[i]=="{1}")
		{
			 j++;	
		}	
}

if(flag==0&&j==2)
{
  flag=1;
}
}

void set_flag_b(thread_ctx_t *thread_ctx,  uint32_t dst)
{
j=0;

for (i=0;i<1;i++)
{
	s[i]=tag_sprint(RTAG[dst][i]);

	if(s[i]=="{1}")
		{
			 j++;	
		}	
}

if(flag==0&&j==1)
{
  flag=1;
}
}
static void PIN_FAST_ANALYSIS_CALL
r2r_print_tag(ADDRINT ins_address, thread_ctx_t *thread_ctx, uint32_t dst, uint32_t dst_val, uint32_t src, uint32_t src_val)
{
set_flag(thread_ctx,dst);
set_flag(thread_ctx,src);
}

static void PIN_FAST_ANALYSIS_CALL
r2m_print_tag(ADDRINT ins_address, thread_ctx_t *thread_ctx, uint32_t dst, uint32_t dst_val, ADDRINT src)
{
set_flag(thread_ctx,dst);
}

static void PIN_FAST_ANALYSIS_CALL
r2i_print_tag(ADDRINT ins_address, thread_ctx_t *thread_ctx,  uint32_t dst, uint32_t dst_val, ADDRINT imm_val)
{
set_flag_imm(thread_ctx,dst,imm_val);
		
}

static void PIN_FAST_ANALYSIS_CALL
r2r_print_tag_w(ADDRINT ins_address, thread_ctx_t *thread_ctx, uint32_t dst, uint16_t dst_val, uint32_t src, uint16_t src_val)
{
set_flag_w(thread_ctx,dst);
set_flag_w(thread_ctx,src);
}


static void PIN_FAST_ANALYSIS_CALL
r2i_print_tag_w(ADDRINT ins_address, thread_ctx_t *thread_ctx,  uint32_t dst, uint16_t dst_val, ADDRINT imm_val)
{
set_flag_imm_w(thread_ctx,dst,imm_val);
}

static void PIN_FAST_ANALYSIS_CALL
r2m_print_tag_w(ADDRINT ins_address, thread_ctx_t *thread_ctx, uint32_t dst, uint16_t dst_val, ADDRINT src)
{
set_flag_w(thread_ctx,dst);
}


static void PIN_FAST_ANALYSIS_CALL
r2r_print_tag_opb_l(ADDRINT ins_address, thread_ctx_t *thread_ctx, uint32_t dst, uint8_t dst_val, uint32_t src, uint8_t src_val)
{
set_flag_b(thread_ctx,dst);
set_flag_b(thread_ctx,src);
}

static void PIN_FAST_ANALYSIS_CALL
r2i_print_tag_8_bl(ADDRINT ins_address, thread_ctx_t *thread_ctx,  uint32_t dst, uint8_t dst_val, ADDRINT imm_val)
{
set_flag_imm_b(thread_ctx,dst,imm_val);

}

static void PIN_FAST_ANALYSIS_CALL
r2m_print_tag_8_bl(ADDRINT ins_address, thread_ctx_t *thread_ctx, uint32_t dst, uint8_t dst_val, ADDRINT src)
{
set_flag_b(thread_ctx,dst);


}



static void PIN_FAST_ANALYSIS_CALL
m2i_print_tag_l(ADDRINT ins_address, ADDRINT src, ADDRINT imm_val)
{

}

static void PIN_FAST_ANALYSIS_CALL
m2i_print_tag_w(ADDRINT ins_address, ADDRINT src, ADDRINT imm_val)
{

}
static void PIN_FAST_ANALYSIS_CALL
m2i_print_tag_b(ADDRINT ins_address, ADDRINT src, ADDRINT imm_val)
{

}


	
void dta_instrument_cmp(INS ins)
{
	REG reg_dst, reg_src;
	
	if (INS_OperandIsReg(ins, OP_0))
		{
			reg_dst = INS_OperandReg(ins, OP_0);
			if (REG_is_gr32(reg_dst)) 
				{
					  /* second operand is register */
					  if (INS_OperandIsReg(ins, OP_1))
						{
						    /* case: cmp reg reg */ 
							reg_src = INS_OperandReg(ins, OP_1);
				            INS_InsertCall(ins,
                                                IPOINT_BEFORE,
                                                (AFUNPTR)r2r_print_tag,
                                                IARG_FAST_ANALYSIS_CALL,
												IARG_INST_PTR,
                                                IARG_REG_VALUE, thread_ctx_ptr,
                                                IARG_UINT32, REG32_INDX(reg_dst),
                                                IARG_REG_VALUE, reg_dst,
                                                IARG_UINT32, REG32_INDX(reg_src),
                                                IARG_REG_VALUE, reg_src,
                                                IARG_END);
						}
						/* 	case: cmp reg,imm */
				 	 else if (INS_OperandIsImmediate(ins, OP_1))
				  		{
							INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)r2i_print_tag,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_REG_VALUE, thread_ctx_ptr,
								IARG_UINT32, REG32_INDX(reg_dst),
								IARG_REG_VALUE, reg_dst,
								IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
								IARG_END);

				  		}
						/* case: cmp reg,mem  */
				 	else if(INS_OperandIsMemory(ins, OP_1))
				   		{
							INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)r2m_print_tag,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_REG_VALUE, thread_ctx_ptr,
								IARG_UINT32, REG32_INDX(reg_dst),
								IARG_REG_VALUE, reg_dst,
								IARG_MEMORYREAD_EA,
								IARG_END);
				  	   }
				}
			else if (REG_is_gr16(reg_dst)) 
				{
					  if (INS_OperandIsReg(ins, OP_1))
					  	{
							reg_src = INS_OperandReg(ins, OP_1);
							/* cmp reg reg case */
			         	    INS_InsertCall(ins,
                                                IPOINT_BEFORE,
                                                (AFUNPTR)r2r_print_tag_w,
                                                IARG_FAST_ANALYSIS_CALL,
												IARG_INST_PTR,
                                                IARG_REG_VALUE, thread_ctx_ptr,
                                                IARG_UINT32, REG16_INDX(reg_dst),
                                                IARG_REG_VALUE, reg_dst,
                                                IARG_UINT32, REG16_INDX(reg_src),
                                                IARG_REG_VALUE, reg_src,
                                                IARG_END);
				   		}
				  		/* cmp reg imm case */
				  	else if (INS_OperandIsImmediate(ins, OP_1))
				  		{
						INS_InsertCall(ins,
							IPOINT_BEFORE,
							(AFUNPTR)r2i_print_tag_w,
							IARG_FAST_ANALYSIS_CALL,
							IARG_INST_PTR,
							IARG_REG_VALUE, thread_ctx_ptr,
							IARG_UINT32, REG16_INDX(reg_dst),
							IARG_REG_VALUE, reg_dst,
							IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
							IARG_END);
				  		}
				  		/* cmp reg mem case */
				    else if(INS_OperandIsMemory(ins, OP_1))
				    	{
							INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)r2m_print_tag_w,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_REG_VALUE, thread_ctx_ptr,
								IARG_UINT32, REG16_INDX(reg_dst),
								IARG_REG_VALUE, reg_dst,
								IARG_MEMORYREAD_EA,
								IARG_END);
				  		}
				}
			else if (REG_is_gr8(reg_dst)) 
				{
					  if (INS_OperandIsReg(ins, OP_1))
						{
							reg_src = INS_OperandReg(ins, OP_1);
							/* cmp reg reg case */

							if (REG_is_Lower8(reg_dst) &&
								REG_is_Lower8(reg_src))
								{	
									/* lower 8-bit registers */
									INS_InsertCall(ins,
										IPOINT_BEFORE,
										(AFUNPTR)r2r_print_tag_opb_l,
										IARG_FAST_ANALYSIS_CALL,
										IARG_INST_PTR,
										IARG_REG_VALUE, thread_ctx_ptr,
										IARG_UINT32, REG8_INDX(reg_dst),
										IARG_REG_VALUE, reg_dst,
										IARG_UINT32, REG8_INDX(reg_src),
										IARG_REG_VALUE, reg_src,
										IARG_END);
								}
							else if(REG_is_Upper8(reg_dst) &&
							REG_is_Upper8(reg_src))
								{
									/* upper 8-bit registers */
									INS_InsertCall(ins,
										IPOINT_BEFORE,
										(AFUNPTR)r2r_print_tag_opb_l,
										IARG_FAST_ANALYSIS_CALL,
										IARG_INST_PTR,
										IARG_REG_VALUE, thread_ctx_ptr,
										IARG_UINT32, REG8_INDX(reg_dst),
										IARG_REG_VALUE, reg_dst,
										IARG_UINT32, REG8_INDX(reg_src),
										IARG_REG_VALUE, reg_src,
										IARG_END);
								}
							else if (REG_is_Lower8(reg_dst))
								{
									/* 
						 			* destination register is a
						 			* lower 8-bit register and
						 			* source register is an upper
									* 8-bit register
						 			*/
									INS_InsertCall(ins,
										IPOINT_BEFORE,
										(AFUNPTR)r2r_print_tag_opb_l,
										IARG_FAST_ANALYSIS_CALL,
										IARG_INST_PTR,
										IARG_REG_VALUE, thread_ctx_ptr,
										IARG_UINT32, REG8_INDX(reg_dst),
										IARG_REG_VALUE, reg_dst,
										IARG_UINT32, REG8_INDX(reg_src),
										IARG_REG_VALUE, reg_src,
										IARG_END);
								}
							else
									/* 
						 			* destination register is an
						 			* upper 8-bit register and
						 			* source register is a lower
						 			* 8-bit register
						 			*/
						
						 		{
									INS_InsertCall(ins,
										IPOINT_BEFORE,
										(AFUNPTR)r2r_print_tag_opb_l,
										IARG_FAST_ANALYSIS_CALL,
										IARG_INST_PTR,
										IARG_REG_VALUE, thread_ctx_ptr,
										IARG_UINT32, REG8_INDX(reg_dst),
										IARG_REG_VALUE, reg_dst,
										IARG_UINT32, REG8_INDX(reg_src),
										IARG_REG_VALUE, reg_src,
										IARG_END);
								}
				  }
				  /*
					cmp reg imm case
				 */
				  else if (INS_OperandIsImmediate(ins, OP_1))
						{
							if (REG_is_Lower8(reg_dst))
								{
					  				INS_InsertCall(ins,
									IPOINT_BEFORE,
									(AFUNPTR)r2i_print_tag_8_bl,
									IARG_FAST_ANALYSIS_CALL,
									IARG_INST_PTR,
									IARG_REG_VALUE, thread_ctx_ptr,
									IARG_UINT32, REG8_INDX(reg_dst),
									IARG_REG_VALUE, reg_dst,
									IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
									IARG_END);
				     			}
							else if (REG_is_Upper8(reg_dst))
								{
					  				INS_InsertCall(ins,
									IPOINT_BEFORE,
									(AFUNPTR)r2i_print_tag_8_bl,
									IARG_FAST_ANALYSIS_CALL,
									IARG_INST_PTR,
									IARG_REG_VALUE, thread_ctx_ptr,
									IARG_UINT32, REG8_INDX(reg_dst),
									IARG_REG_VALUE, reg_dst,
									IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
									IARG_END);
				     			}
				  		}
				  /*
					cmp reg mem case
				  */
				  	else if(INS_OperandIsMemory(ins, OP_1))
						{
							if (REG_is_Lower8(reg_dst))
								{
								  INS_InsertCall(ins,
									IPOINT_BEFORE,
									(AFUNPTR)r2m_print_tag_8_bl,
									IARG_FAST_ANALYSIS_CALL,
									IARG_INST_PTR,
									IARG_REG_VALUE, thread_ctx_ptr,
									IARG_UINT32, REG8_INDX(reg_dst),
									IARG_REG_VALUE, reg_dst,
									IARG_MEMORYREAD_EA,
									IARG_END);
					 			}
							if (REG_is_Upper8(reg_dst))
								{
					  				INS_InsertCall(ins,
									IPOINT_BEFORE,
									(AFUNPTR)r2m_print_tag_8_bl,
									IARG_FAST_ANALYSIS_CALL,
									IARG_INST_PTR,
									IARG_REG_VALUE, thread_ctx_ptr,
									IARG_UINT32, REG8_INDX(reg_dst),
									IARG_REG_VALUE, reg_dst,
									IARG_MEMORYREAD_EA,
									IARG_END);
					 			}

				  		}
				}
		}
		else if (INS_OperandIsMemory(ins, OP_0)) {
				//cmp mem reg
				  if (INS_OperandIsReg(ins, OP_1))
				  	{
						reg_src = INS_OperandReg(ins, OP_1);
						if (REG_is_gr32(reg_src))
							 {
								INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)r2m_print_tag,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_REG_VALUE, thread_ctx_ptr,
								IARG_UINT32, REG32_INDX(reg_src),
								IARG_REG_VALUE, reg_src,
								IARG_MEMORYREAD_EA,
								IARG_END);
							}
						else if (REG_is_gr16(reg_src))
							 {
								INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)r2m_print_tag_w,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_REG_VALUE, thread_ctx_ptr,
								IARG_UINT32, REG16_INDX(reg_src),
								IARG_REG_VALUE, reg_src,
								IARG_MEMORYREAD_EA,
								IARG_END);
							}
						else if (REG_is_gr8(reg_src)) 
							{
								if (REG_is_Lower8(reg_src))
									{
						  			 INS_InsertCall(ins,
										IPOINT_BEFORE,
										(AFUNPTR)r2m_print_tag_8_bl,
										IARG_FAST_ANALYSIS_CALL,
										IARG_INST_PTR,
										IARG_REG_VALUE, thread_ctx_ptr,
										IARG_UINT32, REG8_INDX(reg_src),
										IARG_REG_VALUE, reg_src,
										IARG_MEMORYREAD_EA,
										IARG_END);
									}
								else if (REG_is_Upper8(reg_src))
									{
						  
						  			INS_InsertCall(ins,
										IPOINT_BEFORE,
										(AFUNPTR)r2m_print_tag_8_bl,
										IARG_FAST_ANALYSIS_CALL,
										IARG_INST_PTR,
										IARG_REG_VALUE, thread_ctx_ptr,
										IARG_UINT32, REG8_INDX(reg_src),
										IARG_REG_VALUE, reg_src,
										IARG_MEMORYREAD_EA,
										IARG_END);
									}
							}
				  

 				else if (INS_OperandIsImmediate(ins, OP_1))
 					{
					/* 32-bit operand */
						if (INS_OperandWidth(ins, OP_1) == MEM_LONG_LEN)
							{
							
								INS_InsertCall(ins,
									IPOINT_BEFORE,
									(AFUNPTR)m2i_print_tag_l,
									IARG_FAST_ANALYSIS_CALL,
									IARG_INST_PTR,
									IARG_MEMORYREAD_EA,
									IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
									IARG_END);
							}
					/* 16-bit operand */
					else if (INS_OperandWidth(ins, OP_1) == MEM_WORD_LEN)
							{
							INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)m2i_print_tag_w,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_MEMORYREAD_EA,
								IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
								IARG_END);
							}		
					/* 8-bit operand */
					else
						{
							INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)m2i_print_tag_b,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_MEMORYREAD_EA,
								IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
								IARG_END);
						}
				  }
			}
	}			  			

}



/*void Instruction_trace(INS ins, VOID *v)
{
	ADDRINT insaddr=INS_Address(ins);
	std::string disass(INS_Disassemble(ins));
	cout<<insaddr<<" "<<disass<<endl;
}*/
void Fini(INT32 cde, void *v)
{
	double b=0;
	OutFile.setf(ios::showbase);
	OutFile<< "Ins_count_tainted_cmp:  " << ins_count << endl;
	OutFile<< "Ins_count_exit:  " << ins_count_exit << endl;
    
    
		
	b=((double)ins_count / (double)ins_count_exit)*100;
    printf("%lf\n",b);
	
	
	if(b==0)
	{
	 	OutFile<<"Sorry...intended files are not here!"<<endl;
	}
	if(b<2 && b!=0)
	{
		
		OutFile<<"Malicious"<<endl;
	}
	else
	{
		
		OutFile<<"Malicious"<<endl;
	}
	
	OutFile<< "The percentage is "<<b<<endl;
    OutFile.close();
}

int main(int argc, char * argv[])
{   
    
    if (PIN_Init(argc, argv)) return 1;
    PIN_InitSymbols(); /* initialize symbol processing */ 
	OutFile.open(KnobOutputFile.Value().c_str());
    
    if (unlikely(libdft_init() != 0))
		return 1;
    
    INS_AddInstrumentFunction(Instruction,0);
    //INS_AddInstrumentFunction(Instruction_trace,0);     
    (void)ins_set_post(&ins_desc[XED_ICLASS_CMP], dta_instrument_cmp);
    
    
    
    
    /* Instrument system call of open, close and read  */
    
    (void)syscall_set_post(&syscall_desc[__NR_open],post_open_hook);
    (void)syscall_set_post(&syscall_desc[__NR_close], post_close_hook);
    (void)syscall_set_post(&syscall_desc[__NR_read], post_read_hook);
   
 
    PIN_AddFiniFunction(Fini,0);
    PIN_StartProgram();
 
    libdft_die(); 
    return 0;
	
}
