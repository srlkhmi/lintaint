#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <time.h> 
#include <errno.h>
#include <inttypes.h>
#include <iostream>
#include <fstream>
#include <map>
#include <iomanip>
#include <algorithm>
#include <vector>
#include <set>
#include "pin.H"
#include "libdft_api.h"
#include "libdft_core.h"
#include "syscall_desc.h"
#include "tagmap_custom.h"
#include "tagmap.h"
#include "tag_traits.h"
#include "branch_pred.h"


#define DLIB_SUFF	".so"
#define DLIB_SUFF_ALT	".so."
#define WORD_LEN	4

extern syscall_desc_t syscall_desc[SYSCALL_MAX];
/* ins descriptors */
extern ins_desc_t ins_desc[XED_ICLASS_LAST];
UINT32 	n;
//Descriptors
static set<int> fdset;

std::map <ADDRINT, std::string> instructions;

static UINT64 ins_count = 0;
static UINT64 ins_count_exit = 0;
static int flag=0;

// Required for getting the context.
extern REG thread_ctx_ptr;

std::ofstream OutFile;
KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool",
    "o", "time_hook_output.txt", "specify output file name");
    
    
string s[4];
int i;
int j=0;

// Shorthand for accessing register taint. thread_ctx is expected
// to be defined in the context where the shorthand is used.
#define RTAG thread_ctx->vcpu.gpr

static KNOB<string> TrackStdin(KNOB_MODE_WRITEONCE, "pintool", "stdin",
	"0", "Taint data originating from stdin.");
void post_time_hook(syscall_ctx_t *ctx) 
{

	cout<<"Time system call is there"<<endl;

	// see system call manual (e.g. man 2 time) for proper error condition
	if (unlikely((time_t)(ctx->ret) < 0)) {
		std::cout << "time syscall failed!" << std::endl;
		return;
	}

	// get the thread context - taken from libdft_api.c:sysexit_save()
	// PIN_GetContextReg requires two arguments:
	//		- the first is provided as ctx->aux from the function argument
	//		- the second is a globel defined in libdft_api.c - we only need
	//		  to add an extern declaration
	thread_ctx_t *thread_ctx = (thread_ctx_t *)PIN_GetContextReg((CONTEXT *)ctx->aux, thread_ctx_ptr);
	   
	RTAG[GPR_EAX][0] = {777};
	RTAG[GPR_EAX][1] = {777};
	RTAG[GPR_EAX][2] = {777};
	RTAG[GPR_EAX][3] = {777};
}

void post_gettimeofday_hook(syscall_ctx_t *ctx) 
{

cout<<" There are get time of the day "<<endl;
}

VOID ins_cout_exit() 
{ 
	ins_count_exit++; 
}

VOID ins_count_tainted_cmp() 
{ 
	ins_count++; 
}

VOID Instruction(INS ins, VOID *v)
{
    // Insert a call to docount before every instruction, no arguments are passed
    
    INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)ins_cout_exit, IARG_END);
    
    if(flag==1)
    {
    INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)ins_count_tainted_cmp, IARG_END);
    }
}

void set_flag_imm(thread_ctx_t *thread_ctx,  uint32_t dst,ADDRINT imm_val)
{
j=0;
if(imm_val!=4294967295)
{
for (i=0;i<4;i++)
{
	s[i]=tag_sprint(RTAG[dst][i]);

	if(s[i]=="{777}")
		{
			 j++;	
		}	
}
}
if(flag==0&&j==4)
{
  flag=1;
}
}

void set_flag_imm_w(thread_ctx_t *thread_ctx,  uint32_t dst,ADDRINT imm_val)
{
j=0;
if(imm_val!=4294967295)
{
for (i=0;i<2;i++)
{
	s[i]=tag_sprint(RTAG[dst][i]);

	if(s[i]=="{777}")
		{
			 j++;	
		}	
}
}
if(flag==0&&j==2)
{
  flag=1;
}


}

void set_flag_imm_b(thread_ctx_t *thread_ctx,  uint32_t dst,ADDRINT imm_val)
{
j=0;
if(imm_val!=4294967295)
{
for (i=0;i<1;i++)
{
	s[i]=tag_sprint(RTAG[dst][i]);

	if(s[i]=="{777}")
		{
			 j++;	
		}	
}
}
if(flag==0&&j==1)
{
  flag=1;
}
}

void set_flag(thread_ctx_t *thread_ctx,  uint32_t dst)
{
j=0;

for (i=0;i<4;i++)
{
	s[i]=tag_sprint(RTAG[dst][i]);

	if(s[i]=="{777}")
		{
			 j++;	
		}	
}

if(flag==0&&j==4)
{
  flag=1;
}
}

void set_flag_w(thread_ctx_t *thread_ctx,  uint32_t dst)
{
j=0;

for (i=0;i<2;i++)
{
	s[i]=tag_sprint(RTAG[dst][i]);

	if(s[i]=="{777}")
		{
			 j++;	
		}	
}

if(flag==0&&j==2)
{
  flag=1;
}
}

void set_flag_b(thread_ctx_t *thread_ctx,  uint32_t dst)
{
j=0;

for (i=0;i<1;i++)
{
	s[i]=tag_sprint(RTAG[dst][i]);

	if(s[i]=="{777}")
		{
			 j++;	
		}	
}

if(flag==0&&j==1)
{
  flag=1;
}
}
static void PIN_FAST_ANALYSIS_CALL
r2r_print_tag(ADDRINT ins_address, thread_ctx_t *thread_ctx, uint32_t dst, uint32_t dst_val, uint32_t src, uint32_t src_val)
{
set_flag(thread_ctx,dst);
set_flag(thread_ctx,src);
}

static void PIN_FAST_ANALYSIS_CALL
r2m_print_tag(ADDRINT ins_address, thread_ctx_t *thread_ctx, uint32_t dst, uint32_t dst_val, ADDRINT src)
{
set_flag(thread_ctx,dst);

}

static void PIN_FAST_ANALYSIS_CALL
r2i_print_tag(ADDRINT ins_address, thread_ctx_t *thread_ctx,  uint32_t dst, uint32_t dst_val, ADDRINT imm_val)
{
set_flag_imm(thread_ctx,dst,imm_val);
	
		
}

static void PIN_FAST_ANALYSIS_CALL
r2r_print_tag_w(ADDRINT ins_address, thread_ctx_t *thread_ctx, uint32_t dst, uint16_t dst_val, uint32_t src, uint16_t src_val)
{
set_flag_w(thread_ctx,dst);
set_flag_w(thread_ctx,src);
}


static void PIN_FAST_ANALYSIS_CALL
r2i_print_tag_w(ADDRINT ins_address, thread_ctx_t *thread_ctx,  uint32_t dst, uint16_t dst_val, ADDRINT imm_val)
{
set_flag_imm_w(thread_ctx,dst,imm_val);
}

static void PIN_FAST_ANALYSIS_CALL
r2m_print_tag_w(ADDRINT ins_address, thread_ctx_t *thread_ctx, uint32_t dst, uint16_t dst_val, ADDRINT src)
{
set_flag_w(thread_ctx,dst);

}


static void PIN_FAST_ANALYSIS_CALL
r2r_print_tag_opb_l(ADDRINT ins_address, thread_ctx_t *thread_ctx, uint32_t dst, uint8_t dst_val, uint32_t src, uint8_t src_val)
{
set_flag_b(thread_ctx,dst);
set_flag_b(thread_ctx,src);
}

static void PIN_FAST_ANALYSIS_CALL
r2i_print_tag_8_bl(ADDRINT ins_address, thread_ctx_t *thread_ctx,  uint32_t dst, uint8_t dst_val, ADDRINT imm_val)
{
set_flag_imm_b(thread_ctx,dst,imm_val);

}

static void PIN_FAST_ANALYSIS_CALL
r2m_print_tag_8_bl(ADDRINT ins_address, thread_ctx_t *thread_ctx, uint32_t dst, uint8_t dst_val, ADDRINT src)
{
set_flag_b(thread_ctx,dst);


}



static void PIN_FAST_ANALYSIS_CALL
m2i_print_tag_l(ADDRINT ins_address, ADDRINT src, ADDRINT imm_val)
{

}

static void PIN_FAST_ANALYSIS_CALL
m2i_print_tag_w(ADDRINT ins_address, ADDRINT src, ADDRINT imm_val)
{

}
static void PIN_FAST_ANALYSIS_CALL
m2i_print_tag_b(ADDRINT ins_address, ADDRINT src, ADDRINT imm_val)
{

}


	
void dta_instrument_cmp(INS ins)
{
	REG reg_dst, reg_src;
	
	if (INS_OperandIsReg(ins, OP_0))
		{
			reg_dst = INS_OperandReg(ins, OP_0);
			if (REG_is_gr32(reg_dst)) 
				{
					  /* second operand is register */
					  if (INS_OperandIsReg(ins, OP_1))
						{
						    /* case: cmp reg reg */ 
							reg_src = INS_OperandReg(ins, OP_1);
				            INS_InsertCall(ins,
                                                IPOINT_BEFORE,
                                                (AFUNPTR)r2r_print_tag,
                                                IARG_FAST_ANALYSIS_CALL,
												IARG_INST_PTR,
                                                IARG_REG_VALUE, thread_ctx_ptr,
                                                IARG_UINT32, REG32_INDX(reg_dst),
                                                IARG_REG_VALUE, reg_dst,
                                                IARG_UINT32, REG32_INDX(reg_src),
                                                IARG_REG_VALUE, reg_src,
                                                IARG_END);
						}
						/* 	case: cmp reg,imm */
				 	 else if (INS_OperandIsImmediate(ins, OP_1))
				  		{
							INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)r2i_print_tag,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_REG_VALUE, thread_ctx_ptr,
								IARG_UINT32, REG32_INDX(reg_dst),
								IARG_REG_VALUE, reg_dst,
								IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
								IARG_END);

				  		}
						/* case: cmp reg,mem  */
				 	else if(INS_OperandIsMemory(ins, OP_1))
				   		{
							INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)r2m_print_tag,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_REG_VALUE, thread_ctx_ptr,
								IARG_UINT32, REG32_INDX(reg_dst),
								IARG_REG_VALUE, reg_dst,
								IARG_MEMORYREAD_EA,
								IARG_END);
				  	   }
				}
			else if (REG_is_gr16(reg_dst)) 
				{
					  if (INS_OperandIsReg(ins, OP_1))
					  	{
							reg_src = INS_OperandReg(ins, OP_1);
							/* cmp reg reg case */
			         	    INS_InsertCall(ins,
                                                IPOINT_BEFORE,
                                                (AFUNPTR)r2r_print_tag_w,
                                                IARG_FAST_ANALYSIS_CALL,
												IARG_INST_PTR,
                                                IARG_REG_VALUE, thread_ctx_ptr,
                                                IARG_UINT32, REG16_INDX(reg_dst),
                                                IARG_REG_VALUE, reg_dst,
                                                IARG_UINT32, REG16_INDX(reg_src),
                                                IARG_REG_VALUE, reg_src,
                                                IARG_END);
				   		}
				  		/* cmp reg imm case */
				  	else if (INS_OperandIsImmediate(ins, OP_1))
				  		{
						INS_InsertCall(ins,
							IPOINT_BEFORE,
							(AFUNPTR)r2i_print_tag_w,
							IARG_FAST_ANALYSIS_CALL,
							IARG_INST_PTR,
							IARG_REG_VALUE, thread_ctx_ptr,
							IARG_UINT32, REG16_INDX(reg_dst),
							IARG_REG_VALUE, reg_dst,
							IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
							IARG_END);
				  		}
				  		/* cmp reg mem case */
				    else if(INS_OperandIsMemory(ins, OP_1))
				    	{
							INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)r2m_print_tag_w,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_REG_VALUE, thread_ctx_ptr,
								IARG_UINT32, REG16_INDX(reg_dst),
								IARG_REG_VALUE, reg_dst,
								IARG_MEMORYREAD_EA,
								IARG_END);
				  		}
				}
			else if (REG_is_gr8(reg_dst)) 
				{
					  if (INS_OperandIsReg(ins, OP_1))
						{
							reg_src = INS_OperandReg(ins, OP_1);
							/* cmp reg reg case */

							if (REG_is_Lower8(reg_dst) &&
								REG_is_Lower8(reg_src))
								{	
									/* lower 8-bit registers */
									INS_InsertCall(ins,
										IPOINT_BEFORE,
										(AFUNPTR)r2r_print_tag_opb_l,
										IARG_FAST_ANALYSIS_CALL,
										IARG_INST_PTR,
										IARG_REG_VALUE, thread_ctx_ptr,
										IARG_UINT32, REG8_INDX(reg_dst),
										IARG_REG_VALUE, reg_dst,
										IARG_UINT32, REG8_INDX(reg_src),
										IARG_REG_VALUE, reg_src,
										IARG_END);
								}
							else if(REG_is_Upper8(reg_dst) &&
							REG_is_Upper8(reg_src))
								{
									/* upper 8-bit registers */
									INS_InsertCall(ins,
										IPOINT_BEFORE,
										(AFUNPTR)r2r_print_tag_opb_l,
										IARG_FAST_ANALYSIS_CALL,
										IARG_INST_PTR,
										IARG_REG_VALUE, thread_ctx_ptr,
										IARG_UINT32, REG8_INDX(reg_dst),
										IARG_REG_VALUE, reg_dst,
										IARG_UINT32, REG8_INDX(reg_src),
										IARG_REG_VALUE, reg_src,
										IARG_END);
								}
							else if (REG_is_Lower8(reg_dst))
								{
									/* 
						 			* destination register is a
						 			* lower 8-bit register and
						 			* source register is an upper
									* 8-bit register
						 			*/
									INS_InsertCall(ins,
										IPOINT_BEFORE,
										(AFUNPTR)r2r_print_tag_opb_l,
										IARG_FAST_ANALYSIS_CALL,
										IARG_INST_PTR,
										IARG_REG_VALUE, thread_ctx_ptr,
										IARG_UINT32, REG8_INDX(reg_dst),
										IARG_REG_VALUE, reg_dst,
										IARG_UINT32, REG8_INDX(reg_src),
										IARG_REG_VALUE, reg_src,
										IARG_END);
								}
							else
									/* 
						 			* destination register is an
						 			* upper 8-bit register and
						 			* source register is a lower
						 			* 8-bit register
						 			*/
						
						 		{
									INS_InsertCall(ins,
										IPOINT_BEFORE,
										(AFUNPTR)r2r_print_tag_opb_l,
										IARG_FAST_ANALYSIS_CALL,
										IARG_INST_PTR,
										IARG_REG_VALUE, thread_ctx_ptr,
										IARG_UINT32, REG8_INDX(reg_dst),
										IARG_REG_VALUE, reg_dst,
										IARG_UINT32, REG8_INDX(reg_src),
										IARG_REG_VALUE, reg_src,
										IARG_END);
								}
				  }
				  /*
					cmp reg imm case
				 */
				  else if (INS_OperandIsImmediate(ins, OP_1))
						{
							if (REG_is_Lower8(reg_dst))
								{
					  				INS_InsertCall(ins,
									IPOINT_BEFORE,
									(AFUNPTR)r2i_print_tag_8_bl,
									IARG_FAST_ANALYSIS_CALL,
									IARG_INST_PTR,
									IARG_REG_VALUE, thread_ctx_ptr,
									IARG_UINT32, REG8_INDX(reg_dst),
									IARG_REG_VALUE, reg_dst,
									IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
									IARG_END);
				     			}
							else if (REG_is_Upper8(reg_dst))
								{
					  				INS_InsertCall(ins,
									IPOINT_BEFORE,
									(AFUNPTR)r2i_print_tag_8_bl,
									IARG_FAST_ANALYSIS_CALL,
									IARG_INST_PTR,
									IARG_REG_VALUE, thread_ctx_ptr,
									IARG_UINT32, REG8_INDX(reg_dst),
									IARG_REG_VALUE, reg_dst,
									IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
									IARG_END);
				     			}
				  		}
				  /*
					cmp reg mem case
				  */
				  	else if(INS_OperandIsMemory(ins, OP_1))
						{
							if (REG_is_Lower8(reg_dst))
								{
								  INS_InsertCall(ins,
									IPOINT_BEFORE,
									(AFUNPTR)r2m_print_tag_8_bl,
									IARG_FAST_ANALYSIS_CALL,
									IARG_INST_PTR,
									IARG_REG_VALUE, thread_ctx_ptr,
									IARG_UINT32, REG8_INDX(reg_dst),
									IARG_REG_VALUE, reg_dst,
									IARG_MEMORYREAD_EA,
									IARG_END);
					 			}
							if (REG_is_Upper8(reg_dst))
								{
					  				INS_InsertCall(ins,
									IPOINT_BEFORE,
									(AFUNPTR)r2m_print_tag_8_bl,
									IARG_FAST_ANALYSIS_CALL,
									IARG_INST_PTR,
									IARG_REG_VALUE, thread_ctx_ptr,
									IARG_UINT32, REG8_INDX(reg_dst),
									IARG_REG_VALUE, reg_dst,
									IARG_MEMORYREAD_EA,
									IARG_END);
					 			}

				  		}
				}
		}
		else if (INS_OperandIsMemory(ins, OP_0)) {
				//cmp mem reg
				  if (INS_OperandIsReg(ins, OP_1))
				  	{
						reg_src = INS_OperandReg(ins, OP_1);
						if (REG_is_gr32(reg_src))
							 {
								INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)r2m_print_tag,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_REG_VALUE, thread_ctx_ptr,
								IARG_UINT32, REG32_INDX(reg_src),
								IARG_REG_VALUE, reg_src,
								IARG_MEMORYREAD_EA,
								IARG_END);
							}
						else if (REG_is_gr16(reg_src))
							 {
								INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)r2m_print_tag_w,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_REG_VALUE, thread_ctx_ptr,
								IARG_UINT32, REG16_INDX(reg_src),
								IARG_REG_VALUE, reg_src,
								IARG_MEMORYREAD_EA,
								IARG_END);
							}
						else if (REG_is_gr8(reg_src)) 
							{
								if (REG_is_Lower8(reg_src))
									{
						  			 INS_InsertCall(ins,
										IPOINT_BEFORE,
										(AFUNPTR)r2m_print_tag_8_bl,
										IARG_FAST_ANALYSIS_CALL,
										IARG_INST_PTR,
										IARG_REG_VALUE, thread_ctx_ptr,
										IARG_UINT32, REG8_INDX(reg_src),
										IARG_REG_VALUE, reg_src,
										IARG_MEMORYREAD_EA,
										IARG_END);
									}
								else if (REG_is_Upper8(reg_src))
									{
						  
						  			INS_InsertCall(ins,
										IPOINT_BEFORE,
										(AFUNPTR)r2m_print_tag_8_bl,
										IARG_FAST_ANALYSIS_CALL,
										IARG_INST_PTR,
										IARG_REG_VALUE, thread_ctx_ptr,
										IARG_UINT32, REG8_INDX(reg_src),
										IARG_REG_VALUE, reg_src,
										IARG_MEMORYREAD_EA,
										IARG_END);
									}
							}
				  

 				else if (INS_OperandIsImmediate(ins, OP_1))
 					{
					/* 32-bit operand */
						if (INS_OperandWidth(ins, OP_1) == MEM_LONG_LEN)
							{
							
								INS_InsertCall(ins,
									IPOINT_BEFORE,
									(AFUNPTR)m2i_print_tag_l,
									IARG_FAST_ANALYSIS_CALL,
									IARG_INST_PTR,
									IARG_MEMORYREAD_EA,
									IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
									IARG_END);
							}
					/* 16-bit operand */
					else if (INS_OperandWidth(ins, OP_1) == MEM_WORD_LEN)
							{
							INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)m2i_print_tag_w,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_MEMORYREAD_EA,
								IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
								IARG_END);
							}		
					/* 8-bit operand */
					else
						{
							INS_InsertCall(ins,
								IPOINT_BEFORE,
								(AFUNPTR)m2i_print_tag_b,
								IARG_FAST_ANALYSIS_CALL,
								IARG_INST_PTR,
								IARG_MEMORYREAD_EA,
								IARG_ADDRINT, (ADDRINT)INS_OperandImmediate(ins, OP_1),
								IARG_END);
						}
				  }
			}
	}			  			

}


/*void Instruction_trace(INS ins, VOID *v)
{
	ADDRINT insaddr=INS_Address(ins);
	std::string disass(INS_Disassemble(ins));
	cout<<insaddr<<" "<<disass<<endl;
}*/
void Fini(INT32 cde, void *v)
{
	double b=0;
	cout<<"---------------------------------------------------------"<<endl;
    cout<< "Ins_count_tainted_cmp:  " << ins_count << endl;
	cout<< "Ins_count_exit:  " << ins_count_exit << endl;
	cout<<"---------------------------------------------------------"<<endl;
	//cout<<" Input the Minimum number of instruction to check whether the sample is malicious"<<endl;
	OutFile.setf(ios::showbase);
	OutFile<< "Ins_count_tainted_cmp:  " << ins_count << endl;
	OutFile<< "Ins_count_exit:  " << ins_count_exit << endl;
    
    
		
	b=((double)ins_count / (double)ins_count_exit)*100;
    printf("%lf\n",b);
	
	if(b<2)
	{
		cout<<"Malicious"<<endl;
		OutFile<<"Malicious"<<endl;
	}
	else
	{
		cout<<"Not Malicious"<<endl;
		OutFile<<"Not Malicious"<<endl;
	}
	
	OutFile<< "The percentage is "<<b<<endl;
    OutFile.close();
   // cout<<KNOB_BASE::StringKnobSummary()<<endl;
}

int main(int argc, char * argv[])
{   
    
    if (PIN_Init(argc, argv)) return 1;
    PIN_InitSymbols(); /* initialize symbol processing */ 
	OutFile.open(KnobOutputFile.Value().c_str());
    
    if (unlikely(libdft_init() != 0))
		return 1;
    
    INS_AddInstrumentFunction(Instruction,0);
    //INS_AddInstrumentFunction(Instruction_trace,0);     
    (void)ins_set_post(&ins_desc[XED_ICLASS_CMP], dta_instrument_cmp);
    
    
    
    
    /* Instrument system call of open, close, read and time */
    (void)syscall_set_post(&syscall_desc[__NR_time], post_time_hook); //time
    (void)syscall_set_post(&syscall_desc[__NR_gettimeofday], post_gettimeofday_hook);
 
    PIN_AddFiniFunction(Fini,0);
    PIN_StartProgram();
 
    libdft_die(); 
    return 0;
	
}
